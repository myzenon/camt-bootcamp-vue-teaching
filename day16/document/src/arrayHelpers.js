
const sampleArray = [2,3,4,7,9]

// Map
const mapResult = sampleArray.map((eachNumber, index) => {
    return eachNumber * 2
})
// ผลลัพธ์: [4,6,8,14,18]


// Filter
const filterResult = sampleArray.filter((eachNumber, index) => {
    return eachNumber % 2 === 0
})
// ผลลัพธ์: [2,4]


// Find
const findResult = sampleArray.find((eachNumber, index) => {
    return eachNumber % 2 !== 0
})
// ผลลัพธ์: 3


// Reduce
const reduceResult = sampleArray.reduce(
    (sum, eachNumber, index) => {
        sum = sum + eachNumber
        return sum
    },
    0
)
// ผลลัพธ์: 25



// For Each
sampleArray.forEach((eachNumber, index) => {
    console.log(eachNumber)
})








