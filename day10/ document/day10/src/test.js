
// แบบตั้งตัวแปรก่อน
const someNumber = 3
export { someNumber }

// แบบตั้งตัวแปรพร้อม export
export const anyString = 'value'

// แบบ Default
export default anyThing = 'this is my data'




