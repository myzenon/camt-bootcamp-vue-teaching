
// import แบบ ระบุชื่อตัวแปรชัดเจน ตาม export
import { someNumber } from './test'
import { anyString } from './test'
// สามารถรวมกันได้
import { anyString, someNumber } from './test'

// import แบบเอาทุกอย่างที่ export
import * as test from './test'
// ผลลัพธ์
// test = {
//     someNumber: 3,
//     anyString: 'value',
//     default: 'this is my data',
// }

// import แบบ default จาก export default
// ** สามารถใช้ชื่อตัวแปรตอน import แบบไหนก็ได้
import test from './test'
import abc from './test'
// ผลลัพธ์
// test = 'this is my data'
// abc = 'this is my data'






someNumber
anyString
abc
test