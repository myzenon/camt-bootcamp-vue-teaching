


// ส่วนหัวของฟังก์ชัน - Function Header
function ชื่อ(ชื่อตัวแปร_พารามิเตอร์_Parameter, ชื่อตัวแปร) {
    // ส่วนตัวของฟังก์ชัน - Function Body
    return ข้อมูลที่ต้องการส่งกลับ
}



// Function เก็บไนตัวแปรได้
const ชื่อฟังก์ชั่น = function() {
    // ส่วนตัวของฟังก์ชัน - Function Body
}



// Arrow Function
const ชื่อฟังก์ชั่น = (ชื่อตัวแปร, ชื่อตัวแปร) => {
    // ส่วนตัวของฟังก์ชัน - Function Body
}



// Arrow Function แบบ Return ทันที
const ชื่อฟังก์ชั่น = (ชื่อตัวแปร, ชื่อตัวแปร) => ข้อมูลที่ต้องการส่งกลับ







