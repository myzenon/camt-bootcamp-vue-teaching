
ชื่อตัวแปร.ชื่อคีย์
// - หรือ -
ชื่อตัวแปร['ชื่อคีย์']

//  --= จากตัวอย่าง =-

// ต้องการอายุ
person.age
// - หรือ -
person['age']

// ต้องการ ประเทศที่อยู่
person.address.country
// - หรือ -
person['address']['country']



