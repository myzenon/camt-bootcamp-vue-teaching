const me = {
    name: 'Boss',
    age: 26,
    gender: 'male',
    address: {
        postalCode: '50200',
        province: 'Chiang Mai',
        country: 'Thailand'
    }
}

// Show something
console.log(me.address.postalCode)