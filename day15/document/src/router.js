import VueRouter from 'vue-router'
import Create from './pages/Create.vue'
import List from './pages/List.vue'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/create',
            component: Create,
        },
        {
            path: '/',
            component: List,
        }
    ],
})

export default router