const store = {
    state: {
        todos: [],
    },
    addTodo(todo) {
        this.state.todos.push(todo)
        localStorage.setItem('todos', JSON.stringify(this.state.todos))
    },
    initial() {
        const localStorageTodos = localStorage.getItem('todos')
        if (localStorageTodos) {
            const todos = JSON.parse(localStorage.getItem('todos'))
            this.state.todos = todos
        }
    }
}

export default store





