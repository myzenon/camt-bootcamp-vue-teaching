import VueRouter from 'vue-router'
import Home from './Home'
import About from './About'

const router = new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
        },
        {
            path: '/about',
            component: About,
        }
    ],
    mode: 'history',
})

export default router