import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'

// Pages
import Home from './Home.vue'
import About from './About.vue'
import News from './News.vue'

Vue.config.productionTip = false
Vue.use(VueRouter)

const router = new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
        },
        {
            path: '/about',
            component: About,
        },
        {
            path: '/news/:id',
            component: News,
        }
    ],
    mode: 'history',
})

new Vue({
    render: h => h(App),
    router,
}).$mount('#app')




